SELECT 
	Orders.OrderID,
	Orders.OrderDate,
	Customers.ContactName as CustomerName,
	Employees.FirstName + Employees.LastName as EmployeeName
	FROM 
		Orders
				JOIN
			Employees ON Orders.EmployeeID = Employees.EmployeeID
		JOIN 
			Customers ON Orders.CustomerID = Customers.CustomerID
		

	WHERE 
		OrderDate BETWEEN '19970701' AND '19970931'

	ORDER BY 
		Orders.OrderDate ASC,
		Customers.ContactName
		