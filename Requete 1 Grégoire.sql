SELECT *
FROM Products
WHERE CategoryID = 1
OR CategoryID = 3
OR CategoryID = 4
OR CategoryID = 8
ORDER BY CategoryID ASC, UnitPrice DESC


SELECT *
FROM Products
WHERE CategoryID IN (1, 3, 4, 8)
ORDER BY CategoryID ASC, UnitPrice DESC
