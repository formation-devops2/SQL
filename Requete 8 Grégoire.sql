SELECT 
	Orders.OrderID,
	OrderDate,
	concat(e.FirstName,' ', e.LastName) AS Employer,
	concat(chef.FirstName,' ', chef.LastName) as Chef,
	Customers.CompanyName,
	Shippers.CompanyName as Livraison,
	Products.ProductName,
	[Order Details].Quantity
FROM 
	Orders 
	JOIN Employees as e			ON Orders.EmployeeID = e.EmployeeID 
	JOIN Customers				ON Customers.CustomerID = orders.CustomerID 
	LEFT JOIN Employees as chef ON e.ReportsTo = chef.EmployeeID 
	JOIN Shippers				ON Orders.ShipVia = Shippers.ShipperID
	JOIN [Order Details]		ON Orders.OrderID = [Order Details].OrderID
	JOIN Products				ON [Order Details].ProductID = Products.ProductID
WHERE 
	OrderDate BETWEEN '19970701' AND '19970930'
ORDER BY 
	OrderDate ASC,
	Orders.CustomerID ASC