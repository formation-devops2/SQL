SELECT ProductName, Products.CategoryID, UnitPrice, Categories.CategoryName, CompanyName
FROM Products JOIN Categories ON Products.CategoryID = Categories.CategoryID JOIN Suppliers ON Products.SupplierID = Suppliers.SupplierID
ORDER BY CategoryID ASC