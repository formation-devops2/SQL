SELECT ProductName, UnitsInStock * UnitPrice AS Montant_Stock
FROM Products AS e
WHERE e.QuantityPerUnit LIKE '%bottle%'
AND UnitsInStock * UnitPrice < 1000
ORDER BY Montant_Stock