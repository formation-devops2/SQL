SELECT *
FROM Products
WHERE CategoryID IN (
				SELECT DISTINCT CategoryID
				FROM Products
				WHERE CategoryID % 2 = 1)
ORDER BY CategoryID