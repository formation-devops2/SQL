SELECT CompanyName, ContactName ,  PostalCode , City , Country, Address 
FROM dbo.Customers 
UNION
SELECT CompanyName, ContactName ,  PostalCode , City , Country, Address 
FROM dbo.Suppliers 
UNION
SELECT 'Northwind', Concat(FirstName,' ',  LastName) as ContactName , PostalCode, City , Country , Address 
FROM dbo.Employees 
ORDER BY Country ASC, City ASC