SELECT *
FROM Customers
WHERE ContactTitle LIKE '%sales%'
AND Country IN ('France', 'UK', 'USA')
ORDER BY ContactName ASC